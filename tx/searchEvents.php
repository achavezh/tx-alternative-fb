<?php
require __DIR__ . '/vendor/autoload.php';

// Paco vient de faire ça pour vérifier que ça fonctionnait.
// Sinon ça prend un timeout, mais ça doit être parce que la page PHP timeout
// avant que sa propre requête à mobitest.ppom.me timeout, voilà...
// echo 'ok';
// return;

use GraphQL\Query;
use GraphQL\Client;
use GraphQL\Variable;



try{
        $client = new Client(
                'https://mobitest.ppom.me/graphiql'
        );
}catch(ConnectExeption $exception){
        print_r( $exception->getErrorDetails());
}

$gql = (new Query('searchEvents'))
    ->setVariables([new Variable('beginsOn', 'DateTime')])
    ->setVariables([new Variable('endsOn', 'DateTime')])
    ->setVariables([new Variable('term', 'String')])
    
    ->setArguments(['beginsOn' => '$beginsOn'])
    ->setArguments(['endsOn' => '$endsOn'])
    ->setArguments(['term' => '$term'])
    ->setSelectionSet(
        [
        (new Query('elements'))
            ->setSelectionSet(
                [
                    'id',
                	'status',
			        'category',
                    'title',
		            'url',
                	'joinOptions',
                	'description',
            	    'beginsOn',
                    'endsOn',
                    (new Query('physicalAddress'))
                        ->setSelectionSet(
                            [
                                'country',
                                'street',
                                'geom',
                                'postalCode',
                                'locality',
                                'region',
                            ]
                        ),
                   (new Query('picture'))
                        ->setSelectionSet(
                            [
                                'url'
                            ]
                        ),
                    (new Query ('organizerActor'))
                        -> setSelectionSet(
                            [
                                'name'
                            ]
                        )
		        ]
            )
        ]
    );
try {
    if(isset($_REQUEST["submit-recherche"])){
        $beginsOn = $_REQUEST['beginsOn'];
        $endsOn = $_REQUEST['endsOn'];
        $beginsOn .= ":00Z";
        $endsOn .= ":00Z";
        $term = $_REQUEST['term'];
    
        $results = $client->runQuery($gql, true, ['beginsOn' => $beginsOn, 'endsOn' => $endsOn, 'term'=> $term]);
    
    }
}
catch (QueryException $exception) {
    print_r($exception->getErrorDetails());
    exit;
}


?>
<!DOCTYPE html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<title>Résultats</title>
<body>
    <div style="background-color:lightblue; width:100%; height:30px; margin-bottom: 20px"></div>
    <div class="container">
        <h3>Résultats de la recherche</h3>
        <div class ="card-group">
            <?php
            
                foreach($results->getData()['searchEvents']['elements'] as $events):
            ?>
            <div class ="card-group">

                <div class="card" style="width: 18rem; margin : 10px;">
                    <img class="card-img-top" src=<?php print_r($events['picture']['url']);?> alt="tres belle image ">
                    <div class="card-body">
                        <h5 class="card-title">Titre : <?php print_r($events['title']);?> </h5>
                        <h7 class="card-asso"> Asso : <?php print_r($events['organizerActor']['name']);?> </h7>
                        <p class="card-text"> Description : <?php print_r($events['description']);?> </p>
                    </div>
                    <ul class="list-group list-group-flush">
                    <li class="list-group-item">Date de début : <?php
                            $start_date = new DateTime($events['beginsOn']);
                            echo $start_date->format("d-m-Y H:i:s");
                        ?></li>
                        <li class="list-group-item">Date de fin : <?php
                            $end_date = new DateTime($events['endsOn']);
                            echo $end_date->format("d-m-Y H:i:s");
                            ?></li>
                        <li class="list-group-item">Status : <?php print_r($events['status']);?></li>

                        <li class="list-group-item">Localité: 
                        <?php print_r($events['physicalAddress']['locality']);?>
                    </li>
                        <li class="list-group-item">Rue : 
                        <?php print_r($events['physicalAddress']['street']);?>
                    </li>
                    <li class="list-group-item">Code postal: 
                                <?php print_r($events['physicalAddress']['postalCode']);?>
                        </li>
                    <li class="list-group-item">Region : 
                                <?php print_r($events['physicalAddress']['region']);?>
                        </li>
                    
                    <li class="list-group-item">Pays : 
                                <?php print_r($events['physicalAddress']['country']);?>
                        </li>
                    <li class="list-group-item">Coordonnées GPS : 
                                <?php print_r($events['physicalAddress']['geom']);?>
                        </li>
                    
                    </ul>
                    <div class="card-body">
                        <a href=<?php print_r($events['url']); ?> class="card-link">URL</a>
                    </div>
                    </div>
                </div>
            <?php
                endforeach;
            
            ?>
            </div>
        </div>
    </body>
</html>
